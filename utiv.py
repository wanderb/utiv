#!/usr/bin/env python3

import os
import os.path
import sys
import tty
import termios
import urllib
import urllib.request
import tempfile
import math

class urlwrapper(object):
    def __init__(self, path_or_url, reporter_screen=None):
        self.path_or_url = path_or_url
        self.reporter_screen = reporter_screen
        self.spinindex = 0
        self.spinner='|/-\|/-\\'

    def __enter__(self):
        if not urllib.parse.urlparse(self.path_or_url).scheme:
            self.path = self.path_or_url
            self.local = True
            self.path = os.path.abspath(self.path_or_url)
        else:
            self.local = False
            self.fd = tempfile.NamedTemporaryFile()
            self.path = self.fd.name
            urllib.request.urlretrieve(self.path_or_url, self.path, self.reporter)
            if self.reporter_screen is not None:
                self.reporter_screen.clear_screen()
        return self

    def __exit__(self, exceptiontype, value, traceback):
        if not self.local:
            self.fd.close()

    def reporter(self, transferred, block_size, total_size):
        if self.reporter_screen is not None:
            if total_size > 0:
                percentage = transferred / math.ceil(total_size / block_size) * 100
                self.reporter_screen.cursor_home()
                print("Loading %3.0f%% [%s]" % (percentage, self.spinner[self.spinindex % len(self.spinner)]))
                self.spinindex += 1
            else:
                self.reporter_screen.second_screen()
                print("Loading " + self.spinner[self.spinindex % len(spinner)])
                self.spinindex += 1

class urxvtImg(object):
    FIRST_SCREEN = "?1049l"
    SECOND_SCREEN = "?1049h"
    CLEAR = "2J"
    CURSORHOME = "H"
    CURSOROFF = "?25l"
    CURSORON = "?25h"

    def __init__(self):
      self.CSI = "\033["
      self.display_protocol = "\033"
      self.close_protocol = "\a"
      if "screen" in os.environ['TERM']:
          self.display_protocol += "Ptmux;\033\033"
          self.close_protocol += "\033\\"
      self.display_protocol += "]20;"

    def send_escape(self, a):
        self.immediatewrite(self.CSI + a)

    def second_screen(self):
        self.send_escape(self.SECOND_SCREEN)
        self.send_escape(self.CLEAR)
        self.send_escape(self.CURSORHOME)
        self.send_escape(self.CURSOROFF)

    def cursor_home(self):
        self.send_escape(self.CURSORHOME)

    def clear_screen(self):
        self.send_escape(self.CLEAR)
        self.send_escape(self.CURSORHOME)

    def first_screen(self):
        self.send_escape(self.CURSORON)
        self.send_escape(self.FIRST_SCREEN)

    def draw(self, path):
        self.immediatewrite(self.display_protocol + path + ";100x100+0+0:op=keep-aspect:op=center" + self.close_protocol)

    def clear(self):
        self.immediatewrite(self.display_protocol+ ";100x100+1000+1000" + self.close_protocol)

    def getch(self):
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    def immediatewrite(self, a):
        sys.stdout.write(a)
        sys.stdout.flush()

def main():
    try:
        display = urxvtImg()
        display.second_screen()
        for path in sys.argv[1:]:
            with urlwrapper(path, display) as P:
                display.draw(P.path)
                display.getch()
        display.clear()
    finally:
        display.first_screen()

    
if __name__ == "__main__":
    main()
