utiv - URxvt Terminal Image Viewer
===========================

`utiv` can be called with one or more arguments, each argument being a
path to a local image, or an URL of an image. All images formats
supported by `URxvt` background images are supported.
Press any key to cycle to the next image, or to exit when at the last image.


Usage
-----

Put `utiv.py` somehwere in your `${PATH}`, and make it executable.

Execute it like this:
`$ utiv.py <IMAGE-PATH-OR-URL>...

To use utiv from [rtv](https://github.com/michael-lazar/rtv) change the `image/` handlers in your `.mailcap` like this:

```
image/*; utiv.py %s; needsterminal
```

Do not forget to start with `rtv --enable-media`, or to add `enable_media = True` in your `.config/rtv/rtv.config` file.

How it works
------------

`utiv` plays dirty. In order to display an image it:
1. Switches to the secondary terminal screen, clears it, and disables the cursor
2. Set the `URxvt` background to the image (downloaded to a temporary file if
   necessary)
3. On exit it clears the background image and switches back to the primary
   terminal screen.
